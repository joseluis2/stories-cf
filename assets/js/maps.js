import styles from './maps/styles';

function initMap() {
  const coords = {
    lat: -12.064444,
    lng: -77.104227
  };

  let map = new google.maps.Map(document.getElementById('map'), {
    center: coords,
    zoom: 16,
    styles: styles,
  });

  let marker = new google.maps.Marker({
    position: coords,
    map,
    title: 'Colegio Amadeus Mozart',
  });
}

//initMap();