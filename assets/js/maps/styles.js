const grey = '#f5f5f5';
const label = '#2c3e50';
const accentRed = '#ff5972';
const green = '#468c56';
const black = '#1f1f21';
const lightBlue = '#6DBCDB';
const roads = '#eadf8f';
const transit = '#d3ba74';

const styles = [
  {elementType: 'geometry', stylers: [{color: grey}]},
  {elementType: 'labels.text.fill', stylers: [{color: label}]},
  {elementType: 'labels.text.stroke', stylers: [{color: '#ffffff'}]},
];

export default styles;