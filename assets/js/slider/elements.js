const elements = [
  {
    title: 'Lorem Ipsum',
    subtitle: 'Ipsum',
    image: 'https://picsum.photos/1400/1000',
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi harum numquam ea, atque recusandae soluta iste eveniet, quisquam voluptates id dolores reprehenderit blanditiis facere vero assumenda explicabo, quaerat ipsam! Magnam.'
  },
  {
    title: 'Lorem Ipsum 2',
    subtitle: 'Ipsum 2',
    image: 'https://picsum.photos/1400/1000',
    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi harum numquam ea, atque recusandae soluta iste eveniet, quisquam voluptates id dolores reprehenderit blanditiis facere vero assumenda explicabo, quaerat ipsam! Magnam.'
  },
  {
    title: 'Lorem Ipsum 3',
    subtitle: 'Ipsum 3',
    image: 'https://picsum.photos/1400/1000',
    text: 'Este es otro contenido'
  }
];

export default elements;