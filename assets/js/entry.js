import '@fortawesome/fontawesome-free/js/fontawesome'
// import '@fortawesome/fontawesome-free/js/solid'
// import '@fortawesome/fontawesome-free/js/regular'
// import '@fortawesome/fontawesome-free/js/brands'

import '../css/app.scss';

import './slider/sliderDOM';

import './maps';
import './menu';

if (navigator.serviceWorker) {
  navigator.serviceWorker.register('./../sw.js');
}